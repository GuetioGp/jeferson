'use strict';

    module.exports = {
      async up (queryInterface, Sequelize) {
        return queryInterface.bulkInsert('huespedes', [{
          nombres: "JEFERSON LEANDRO", 
          apellidos: "GUETIO MOSQUERA",
          telefono: "3242164194",
          correo: "jeferson.gutio@campusucc.edu.co",
          direccion: "SIBERIA",
          ciudad: "CALDONO",
          Pais: "COLOMBIA",
          createdAt: new Date(),
          updatedAt: new Date()
        }]);
      },
    
      async down (queryInterface, Sequelize) {
        return queryInterface.bulkDelete('huespedes', null, {});
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
      }
    };
