'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('habitaciones', [{
      precio_por_noche: 45000, 
      piso: 4,
      max_personas: 6,
      tiene_cama_bebe: "si",
      tiene_ducha: "Si",
      tiene_bano: "Si",
      tiene_balcon: "Si",
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('habitaciones', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
